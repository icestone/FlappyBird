﻿using UnityEngine;
using System.Collections;

public class ConduitController : MonoBehaviour
{
    public Sprite Conduit1Up { get; set; }
    public Sprite Conduit1Down { get; set; }
    public Sprite Conduit2Up { get; set; }
    public Sprite Conduit2Down { get; set; }
    public float DoubleRate { get; set; }

    public AudioClip GetClip { get; set; }
    public void GenerateConduit()
    {
        var y = Random.Range(6f, 9.6f);
        // 当前是否为双分管道
        var isDouble = Random.Range(0f, 1f) < DoubleRate;
        // 在屏幕最右侧创建两个管道
        var upConduit = new GameObject("conduit_up");
        var upConduitSpriteRenderer = upConduit.AddComponent<SpriteRenderer>();
        upConduitSpriteRenderer.sprite = isDouble ? Conduit2Up : Conduit1Up;
        // 设置初始位置
        upConduit.transform.position = new Vector3(4.3f, y, 0);
        upConduit.transform.parent = this.transform;
        // 让对象自己移动起来
        var upConduitBehaviour = upConduit.AddComponent<ConduitBehaviour>();
        upConduitBehaviour.IsDouble = isDouble;
        upConduitBehaviour.GetClip = GetClip;
        // 添加碰撞体
        upConduit.AddComponent<BoxCollider2D>();

        var downConduit = new GameObject("conduit_down");
        var downConduitSpriteRenderer = downConduit.AddComponent<SpriteRenderer>();
        downConduitSpriteRenderer.sprite = isDouble ? Conduit2Down : Conduit1Down;
        // 设置初始位置
        downConduit.transform.position = new Vector3(4.3f, y - 12.6f, 0);
        downConduit.transform.parent = this.transform;
        // 让对象自己移动起来
        var downConduitBehaviour = downConduit.AddComponent<ConduitBehaviour>();
        downConduitBehaviour.IsDouble = isDouble;
        downConduitBehaviour.GetClip = GetClip;
        // 添加碰撞体
        downConduit.AddComponent<BoxCollider2D>();
    }
}
