﻿using UnityEngine;
using System.Collections;

public class GameData
{
    private GameData() { }

    //private static object _lockHelper = new object();
    private static GameData _instance;
    public static GameData Instance
    {
        get
        {
            // double check
            if (_instance == null)
            {
                lock ("GameData_LockHelper")
                {
                    if (_instance == null)
                    {
                        _instance = new GameData();
                    }
                }
            }
            return _instance;
        }
    }

    public int Score { get; set; }
    public bool IsGameover { get; set; }
}
