﻿using UnityEngine;
using System.Collections;

public class BirdBehaviour : MonoBehaviour
{
    public AudioClip FlyClip { get; set; }
    public AudioClip DieClip { get; set; }
    private Animator animator;
    private float lastPositionY;
    void Start()
    {
        animator = this.GetComponent<Animator>();
    }
    void Update()
    {
        if (this.transform.position.y < lastPositionY)
        {
            animator.enabled = false;
        }
        lastPositionY = this.transform.position.y;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        AudioSource.PlayClipAtPoint(DieClip, this.transform.position);
        GameData.Instance.IsGameover = true;
    }
    public void Fly(float power)
    {
        AudioSource.PlayClipAtPoint(FlyClip, this.transform.position);
        animator.enabled = true;
        rigidbody2D.AddForce(Vector2.up * power, ForceMode2D.Impulse);
        //animator.enabled = false;
    }
}
