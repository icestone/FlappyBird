﻿using UnityEngine;
using System.Collections;

public class ConduitBehaviour : MonoBehaviour
{
    public AudioClip GetClip { get; set; }
    public bool IsDouble { get; set; }
    //public bool IsUp { get; set; }
    private bool isScored;
    // Update is called once per frame
    void Update()
    {
        print(GameData.Instance.Score);
        if (!GameData.Instance.IsGameover)
        {
            this.transform.Translate(-Vector2.right * Time.deltaTime * 5);
            if (this.transform.position.x <= 0 && !isScored && this.name == "conduit_up")
            {
                AudioSource.PlayClipAtPoint(GetClip, Vector3.zero);
                GameData.Instance.Score += IsDouble ? 2 : 1;
                isScored = true;
            }
        }
    }

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
