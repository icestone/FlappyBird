﻿using UnityEngine;
using System.Collections;

public class GroundBehaviour : MonoBehaviour
{
    /// <summary>
    /// 2D物理引擎碰撞检测方法必须2D结尾
    /// </summary>
    /// <param name="col"></param>
    void Update()
    {
        //print(1);
        if (GameData.Instance.IsGameover)
        {
            this.GetComponent<Animator>().enabled = false;
        }
    }
}
