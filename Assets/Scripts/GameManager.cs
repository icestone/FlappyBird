﻿using UnityEngine;
using System.Collections;
 
public class GameManager : MonoBehaviour
{
    public float FlyPower;
    public float Interval;
    public Sprite Conduit1Up;
    public Sprite Conduit1Down;
    public Sprite Conduit2Up;
    public Sprite Conduit2Down;
    public float DoubleRate; // 0-1
    public GameObject Gameover;
    public AudioClip FlyClip;
    public AudioClip DieClip;
    public AudioClip GetClip;

    private BirdBehaviour bridBehaviour;
    private ConduitController conduitController;
    private float timer;
    // Use this for initialization Code Lens
    void Start()
    {
        bridBehaviour = GameObject.Find("Player").GetComponent<BirdBehaviour>();
        bridBehaviour.FlyClip = FlyClip;
        bridBehaviour.DieClip = DieClip;
        conduitController = GameObject.Find("Conduits").GetComponent<ConduitController>();
        conduitController.Conduit1Up = Conduit1Up;
        conduitController.Conduit1Down = Conduit1Down;
        conduitController.Conduit2Up = Conduit2Up;
        conduitController.Conduit2Down = Conduit2Down;
        conduitController.DoubleRate = DoubleRate;
        conduitController.GetClip = GetClip;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameData.Instance.IsGameover)
        {
            Gameover.SetActive(true);
            return;
        }
        timer += Time.deltaTime;
        if (timer >= Interval)
        {
            // 创建上下管道
            conduitController.GenerateConduit();
            timer = 0;
        }
        if (Input.GetMouseButtonDown(0))
        {
            bridBehaviour.Fly(FlyPower);
        }
    }
}
